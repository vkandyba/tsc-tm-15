package ru.vkandyba.tm.controller;

import ru.vkandyba.tm.api.controller.IProjectTaskController;
import ru.vkandyba.tm.api.service.IProjectService;
import ru.vkandyba.tm.api.service.IProjectTaskService;
import ru.vkandyba.tm.api.service.ITaskService;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.util.TerminalUtil;

public class ProjectTaskController implements IProjectTaskController {

    private final IProjectTaskService projectTaskService;

    private final IProjectService projectService;

    private final ITaskService taskService;

    public ProjectTaskController(IProjectTaskService projectTaskService, IProjectService projectService,
                                 ITaskService taskService) {

        this.projectTaskService = projectTaskService;
        this.projectService = projectService;
        this.taskService = taskService;
    }

    @Override
    public void bindTaskToProjectById() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (projectService.findById(projectId) == null) {
            System.out.println("Incorrect values");
            return;
        }
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        if (taskService.findById(taskId) == null) {
            System.out.println("Incorrect values");
            return;
        }
        final Task task = projectTaskService.bindTaskToProjectById(projectId, taskId);
        if (task == null)
            System.out.println("Incorrect values");

    }

    @Override
    public void unbindTaskToProjectById() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (projectService.findById(projectId) == null) {
            System.out.println("Incorrect values");
            return;
        }
        System.out.println("Enter task id");
        final String taskId = TerminalUtil.nextLine();
        if (taskService.findById(taskId) == null) {
            System.out.println("Incorrect values");
            return;
        }
        projectTaskService.unbindTaskToProjectById(projectId, taskId);
    }

    @Override
    public void findAllTaskByProjectId() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (projectService.findById(projectId) == null) {
            System.out.println("Incorrect values");
            return;
        }
        System.out.println(projectTaskService.findAllTaskByProjectId(projectId));
    }

    @Override
    public void removeById() {
        System.out.println("Enter project id");
        final String projectId = TerminalUtil.nextLine();
        if (projectService.findById(projectId) == null) {
            System.out.println("Incorrect values");
            return;
        }
        projectTaskService.removeById(projectId);
    }

}
