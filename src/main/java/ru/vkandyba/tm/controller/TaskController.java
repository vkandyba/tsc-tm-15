package ru.vkandyba.tm.controller;

import ru.vkandyba.tm.api.controller.ITaskController;
import ru.vkandyba.tm.api.service.ITaskService;
import ru.vkandyba.tm.enumerated.Sort;
import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.exception.entity.TaskNotFoundException;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.model.Task;
import ru.vkandyba.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showById() {
        System.out.println("Enter Id");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findById(id);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        showTask(task);
    }

    @Override
    public void removeById() {
        System.out.println("Enter Id");
        final String id = TerminalUtil.nextLine();
        taskService.removeById(id);
        System.out.println("[OK]");
    }

    @Override
    public void removeByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        taskService.removeByName(name);
        System.out.println("[OK]");
    }

    @Override
    public void removeByIndex() {
        System.out.println("Enter Index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        taskService.removeByIndex(index);
        System.out.println("[OK]");
    }

    @Override
    public void updateById() {
        System.out.println("Enter Id");
        final String id = TerminalUtil.nextLine();
        if (taskService.existsById(id)) {
            throw new TaskNotFoundException();
        }
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateById(id, name, description);
        if (task == null) {
            throw new TaskNotFoundException();
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void updateByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!taskService.existsByIndex(index)) {
            throw new TaskNotFoundException();
        }
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateByIndex(index, name, description);
        if (task == null) {
            throw new TaskNotFoundException();
        } else {
            System.out.println("[OK]");
        }
    }

    @Override
    public void showTask(Task task) {
        final List<Task> tasks = taskService.findAll();
        System.out.println(tasks.indexOf(task) + 1 + ". " + task.getName() + " " + task.getId() + ": " + task.getDescription());
    }

    @Override
    public void showByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        if (!taskService.existsByIndex(index)) {
            throw new TaskNotFoundException();
        }
        final Task task = taskService.findByIndex(index);
        showTask(task);
    }

    @Override
    public void showByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.findByName(name);
        if (task == null) {
            throw new TaskNotFoundException();
        }
        showTask(task);
    }

    @Override
    public void showTasks() {
        System.out.println("[LIST TASKS]");
        System.out.println("Enter sort");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        List<Task> tasks;
        if(sort == null || sort.isEmpty()) tasks = taskService.findAll();
        else{
            Sort sortType = Sort.valueOf(sort);
            System.out.println(sortType.getDisplayName());
            tasks = taskService.findAll(sortType.getComparator());
        }
        for (Task task : tasks) {
            System.out.println(tasks.indexOf(task) + 1 + ". " + task.getName() + " " + task.getId() + ": " + task.getDescription());
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[OK]");
    }

    @Override
    public void startById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.startById(id);
        if(task==null) throw new TaskNotFoundException();
    }

    @Override
    public void startByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.startByIndex(index);
        if(task==null) throw new TaskNotFoundException();
    }

    @Override
    public void startByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.startByName(name);
        if(task==null) throw new TaskNotFoundException();
    }

    @Override
    public void finishById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.finishById(id);
        if(task==null) throw new TaskNotFoundException();
    }

    @Override
    public void finishByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Task task = taskService.finishByIndex(index);
        if(task==null) throw new TaskNotFoundException();
    }

    @Override
    public void finishByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        final Task task = taskService.finishByName(name);
        if(task==null) throw new TaskNotFoundException();
    }

    @Override
    public void changeStatusById() {
        System.out.println("Enter id");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = taskService.changeStatusById(id, status);
        if(task==null) throw new TaskNotFoundException();
    }

    @Override
    public void changeStatusByIndex() {
        System.out.println("Enter index");
        final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = taskService.changeStatusByIndex(index, status);
        if(task==null) throw new TaskNotFoundException();
    }

    @Override
    public void changeStatusByName() {
        System.out.println("Enter name");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter status");
        System.out.println(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusValue);
        final Task task = taskService.changeStatusByName(name, status);
        if(task==null) throw new TaskNotFoundException();
    }

}

