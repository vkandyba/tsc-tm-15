package ru.vkandyba.tm.api.service;

import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    List<Task> findAllTaskByProjectId(String projectId);

    Task bindTaskToProjectById(String projectId, String taskId);

    Task unbindTaskToProjectById(String projectId, String taskId);

    void removeAllTaskByProjectId(String projectId);

    Project removeById(String projectId);

}
