package ru.vkandyba.tm.api.service;

import ru.vkandyba.tm.enumerated.Status;
import ru.vkandyba.tm.model.Project;
import ru.vkandyba.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    Boolean existsByIndex(Integer index);

    Boolean existsById(String id);

    Task findById(String id);

    Task findByName(String name);

    Task findByIndex(Integer index);

    Task removeById(String id);

    Task removeByName(String name);

    Task removeByIndex(Integer index);

    Task updateByIndex(Integer index, String name, String description);

    Task updateById(String id, String name, String description);

    Task startById(String id);

    Task startByIndex(Integer index);

    Task startByName(String name);

    Task finishById(String id);

    Task finishByIndex(Integer index);

    Task finishByName(String name);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

    Task changeStatusByName(String name, Status status);

    void create(String name);

    void create(String name, String description);

    Task add(Task task);

    void remove(Task task);

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    void clear();

}
